package com.example.demo.services.interfaces;

import java.util.List;

import com.example.demo.entities.User;

public interface IHelloWorldService {
	public void insertUser(User user);
	
	public List<User> getAll();
}
