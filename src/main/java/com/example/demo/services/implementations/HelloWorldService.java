package com.example.demo.services.implementations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.User;
import com.example.demo.repositories.IUserRepository;
import com.example.demo.services.interfaces.IHelloWorldService;

@Service
public class HelloWorldService implements IHelloWorldService {
	@Autowired
	private IUserRepository userRepository;
	
	public void insertUser(User user) {
		User us = userRepository.save(user);
		System.out.println(us.toString());
	}
	
	public List<User> getAll() {
		List<User> returnList = new ArrayList<>();
		userRepository.findAll().forEach(obj -> returnList.add(obj));
		return returnList;
	}
}
