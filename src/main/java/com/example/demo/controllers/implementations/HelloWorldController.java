package com.example.demo.controllers.implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.controllers.interfaces.IHelloWorldController;
import com.example.demo.entities.User;
import com.example.demo.services.interfaces.IHelloWorldService;

@RestController
public class HelloWorldController implements IHelloWorldController {
	
	@Autowired
	private IHelloWorldService service;

	@Override
	public ResponseEntity<List<User>> getAll() {
		return ResponseEntity.created(null).body(service.getAll());
	}

	@Override
	public ResponseEntity<Void> insertData(@RequestBody User user) {
		service.insertUser(user);
		
		return ResponseEntity.created(null).body(null);
	}

}
