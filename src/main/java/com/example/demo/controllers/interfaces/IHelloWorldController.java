package com.example.demo.controllers.interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.entities.User;

public interface IHelloWorldController {
	@GetMapping("/getAll")
	public ResponseEntity<List<User>> getAll();
	
	@PostMapping("/insert")
	public ResponseEntity<Void> insertData(@RequestBody User user);
}
